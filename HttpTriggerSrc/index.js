module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    const first = req.query.first
    const second = req.query.second

    if (!first || !second) {
        context.res = {
            status: 400,
            body: `Please provide both first and second in query. Now was ${first} and ${second}.`
        };
    } else {
        const sum = first + second
        context.res = {
            status: 200,
            body: `Function triggered successfully. ${first} + ${second} = ${sum}.`
        };
    }

}